(function() {
  'use strict';

  angular
    .module('jd', ['ui.router', 'ui.bootstrap', 'toastr']);

})();
