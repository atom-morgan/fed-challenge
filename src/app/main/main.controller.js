(function() {
  'use strict';

  angular
    .module('jd')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, mainFactory) {
    var vm = this;
    vm.processing = true;

    mainFactory.getMembers()
    .then(function(data) {
      vm.processing = false;
      vm.members = data;
    });

    vm.rowClick = function(index) {
      angular.forEach(vm.members, function(member) {
        if (member.id === index) { vm.selectedMember = member; }
      });
    };

  }
})();
