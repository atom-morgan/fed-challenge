(function() {
  'use strict';

  angular
    .module('jd')
    .factory('mainFactory', mainFactory);

  /** @ngInject */
  function mainFactory($http) {
    var api = 'https://private-a73e-aquentuxsociety.apiary-mock.com/members';

    var service = {
      getMembers: getMembers
    };

    return service;

    function getMembers() {
      return $http.get(api)
        .then(getMembersComplete);

      function getMembersComplete(response) {
        return response.data;
      }
    }
  }
})();
